package src

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

func TestRoutesManager(t *testing.T) {
	e := echo.New()
	e = RoutesManager(e)
	req, err := http.NewRequest(http.MethodGet, "/", nil)
	if err != nil {
		t.Errorf("The request could not be created because of: %v", err)
	}
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	res := rec.Result()
	defer res.Body.Close()

	assert.NoError(t, defaultRoute(c))
	assert.Equal(t, http.StatusOK, rec.Code)
}
