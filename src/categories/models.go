package categories

type Category struct {
	Name        string `json:"CategoryName" required:"true"`
	Description string `json:"CategoryDescription" required:"true"`
}

func CreateCategory() {
	//TODO add a single category
}

func CreateMultipleCategories() {
	//TODO add bulk insert categories
}

func UpdateCategory() {
	//TODO update a single category
}

func DeleteCategory() {
	//TODO delete a category from the DB
}

func GetCategories() {
	//TODO fetch categories
}
