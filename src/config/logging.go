//this package the global configuration for logging
package config

import (
	"github.com/sirupsen/logrus"
)

func init() {
	Logger.SetLevel(logrus.InfoLevel)
}

var Logger = logrus.New()
