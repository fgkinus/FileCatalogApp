/*
This package implements application configuration.
The methods defined are used to read compile time env variables and read time variables
*/

package config

import (
	"os"

	"github.com/jinzhu/configor"

	"github.com/joho/godotenv"
)

type Config struct {
	Database struct {
		Uri string
	}
	Application struct {
		APPName string
		Port    string
		Host    string
	}
}

var Configuration = Config{}

// Simple helper function to read an environment or return a default value
func GetEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}

	return defaultVal
}

// ReadCompileTimeEnv is supposed to read compile time env variables
func ReadCompileTimeEnv(path string) error {
	err := godotenv.Load(path)
	if err != nil {
		return err
	} else {
		return nil
	}
}

// ReadRuntimeConfig is supposed to read the env variables required during runtime including db setup vars ,
// initial credentials etc
func ReadRuntimeConfig(path string) error {
	err := configor.Load(&Configuration, path)
	if err != nil {
		return err
	} else {
		return nil
	}
}
