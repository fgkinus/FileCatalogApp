package config

import (
	"context"

	"github.com/fgkinus/FileCatalogApp/src/config"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var DbClient = &mongo.Client{}

// connect to a mongo db instance that is specified in the uri
func ConnectToDb(dbUri string) (error, *mongo.Client) {
	clientOptions := options.Client().ApplyURI(dbUri)
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		return err, nil
	}
	err = CheckDbConnection(client)
	if err != nil {
		return err, nil
	}
	// log a success message
	config.Logger.Info("Connected to mongo database on " + dbUri)
	return nil, client
}

// check if the database connection is still active else  return error connection
func CheckDbConnection(client *mongo.Client) error {
	// check the connection
	err := client.Ping(context.TODO(), nil)
	if err != nil {
		config.Logger.Error("Database error :", err)
		return err
	} else {
		return nil
	}
}
