package config

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConnectToDb(t *testing.T) {
	// get test db env from the environment
	uri := GetEnv("TEST_DB", "mongodb://127.0.0.1/")
	err, client := ConnectToDb(uri)
	assert.NoError(t, err)
	_ = client.Disconnect(context.TODO())
	err = CheckDbConnection(client)
	assert.Error(t, err)
}
