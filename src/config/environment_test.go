package config

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

func getDir() string {
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	//get parent dir
	dir := filepath.Dir(wd)
	return dir
}

func TestReadCompileTimeEnv(t *testing.T) {
	//get current working directory
	dir := getDir()
	//read env file
	_ = ReadCompileTimeEnv(dir + "/assets/.env")
	appName := GetEnv("APP_NAME", "APP")
	assert.Equal(t, appName, "File Catalog APP")
	// catch error when non existent file is passed
	err := ReadCompileTimeEnv(dir + "/assets/.envs")
	assert.Error(t, err)
}

func TestReadRuntimeConfigWithValidPath(t *testing.T) {
	//get current working directory
	dir := getDir()
	err := ReadRuntimeConfig(dir + "/assets/testConfigFile.yml")
	assert.NoError(t, err)
}

func TestGetEnv(t *testing.T) {
	// test default value is returned
	appName := GetEnv("Random Text", "APP")
	assert.Equal(t, appName, "APP")
}
