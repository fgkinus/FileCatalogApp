package utils

import (
	"github.com/labstack/gommon/log"
)

func LogError(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
