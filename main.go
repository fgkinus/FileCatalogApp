package main

import (
	"os"
	"strings"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/fgkinus/FileCatalogApp/src"
	"gitlab.com/fgkinus/FileCatalogApp/src/config"
)

func init() {
	// read the env
	dir, err := os.Getwd()
	if err != nil {
		config.Logger.Warn(err)
	}
	err = config.ReadCompileTimeEnv(dir + "/.env")
	if err != nil {
		config.Logger.Warn(err)
	}
	err = config.ReadRuntimeConfig(dir + "/config.yml")
	if err != nil {
		config.Logger.Warn(err)
	}
	//database connection
	err, config.DbClient = config.ConnectToDb(config.Configuration.Database.Uri)
	if err != nil {
		config.Logger.Fatal(err)
	}
}

func main() {
	// Echo instance
	var e = echo.New()
	// Middleware
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${time_rfc3339}  ${method}  ${uri}  ${status} \n",
	}))
	e.Use(middleware.Recover())

	// configure the app routes
	src.RoutesManager(e)

	// Start server on the host and port in the config file
	configuration := config.Configuration
	var address = []string{configuration.Application.Host, configuration.Application.Port}
	config.Logger.Fatal(e.Start(strings.Join(address, ":")))
}
